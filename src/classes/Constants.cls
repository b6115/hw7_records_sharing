/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public with sharing class Constants {
    public final static String DEFAULT_ACCESS_LEVEL = 'Read';
}