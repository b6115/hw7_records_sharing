/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public interface IVacancyService {
    void grantAccessFor(Map<Id, Vacancy__c> vacancyMap, String accessLevel);
    void removeAccessFor(Map<Id, Vacancy__c> vacancyMap);
    void updateAccessFor(Map<Id, Vacancy__c> newVacancyMap, Map<Id, Vacancy__c> oldVacancyMap, String accessLevel);
}