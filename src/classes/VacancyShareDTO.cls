/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public with sharing class VacancyShareDTO {
    public Id userId {private set; get; }
    public Id vacancyId {private set; get; }
    public String accessLevel {private set; get; }

    public VacancyShareDTO(Id userId, Id vacancyId) {
        this.userId = userId;
        this.vacancyId = vacancyId;
    }
    public VacancyShareDTO(Id userId, Id vacancyId, String accessLevel) {
        this(userId, vacancyId);
        this.accessLevel = accessLevel;
    }
}