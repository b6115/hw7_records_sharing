/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public with sharing class VacancyShare {
    public void grandAccess(VacancyShareDTO vacancyShare) {
        if (vacancyShare.userId == null
                || vacancyShare.vacancyId == null
                || String.isEmpty(vacancyShare.accessLevel)) {
            return;
        }
        final Vacancy__Share shareObject = composeShareObject(vacancyShare);
        insert shareObject;
    }
    public void grandAccess(List<VacancyShareDTO> vacancyShareList) {
        final List<Vacancy__Share> shareObjectList = new List<Vacancy__Share>();

        for (VacancyShareDTO vacancyShare : vacancyShareList) {
            if (!this.hasPermission(vacancyShare)) {
                Vacancy__Share shareObject = new Vacancy__Share(
                        UserOrGroupId = vacancyShare.userId,
                        ParentId = vacancyShare.vacancyId,
                        AccessLevel = vacancyShare.accessLevel,
                        RowCause = Schema.Vacancy__Share.RowCause.Manual
                );
                shareObjectList.add(shareObject);
            }
        }

        insert shareObjectList;
    }
    public void removeAccess(List<VacancyShareDTO> vacancyShares) {
        for (VacancyShareDTO vacancyShareDTO : vacancyShares) {
            this.removeAccess(vacancyShareDTO);
        }
    }
    public void removeAccess(VacancyShareDTO vacancyShare) {
        this.removeAccess(vacancyShare.userId, vacancyShare.vacancyId);
    }

    public void removeAccess(Id userId, Id vacancyId) {
        List<Vacancy__Share> vacancyShares = [
                SELECT Id
                FROM Vacancy__Share
                WHERE UserOrGroupId = :userId
                AND ParentId = :vacancyId
                AND RowCause = :Schema.Vacancy__Share.RowCause.Manual];
        delete vacancyShares;
    }

    public Boolean hasPermission(VacancyShareDTO share) {
        return [SELECT RecordId, HasReadAccess
                FROM UserRecordAccess
                WHERE UserId = :share.userId
                AND RecordId = :share.vacancyId LIMIT 1].HasReadAccess;
    }

    private Vacancy__Share composeShareObject(VacancyShareDTO vacancyShare) {
        final Vacancy__Share shareObject = new Vacancy__Share(
                UserOrGroupId = vacancyShare.userId,
                ParentId = vacancyShare.vacancyId,
                AccessLevel = vacancyShare.accessLevel,
                RowCause = Schema.Vacancy__Share.RowCause.Manual
        );
        return shareObject;
    }


}