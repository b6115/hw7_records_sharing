/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public with sharing class VacancyTriggerHandler implements IVacancyTriggerHandler {
    private final IVacancyService vacancyService = new VacancyService();

    public void afterInsert(Map<Id, Vacancy__c> vacancyMap) {
        this.vacancyService.grantAccessFor(vacancyMap, Constants.DEFAULT_ACCESS_LEVEL);
    }
    public void beforeUpdate(Map<Id, Vacancy__c> newMap, Map<Id, Vacancy__c> oldMap) {
        this.vacancyService.updateAccessFor(newMap, oldMap, Constants.DEFAULT_ACCESS_LEVEL);
    }
    public static VacancyTriggerHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new VacancyTriggerHandler();
        }
        return INSTANCE;
    }

    private VacancyTriggerHandler() {}

    private static VacancyTriggerHandler INSTANCE = null;
}