/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public interface IVacancyTriggerHandler {
    void afterInsert(Map<Id, Vacancy__c> vacancyMap);
    void beforeUpdate(Map<Id, Vacancy__c> newMap, Map<Id, Vacancy__c> oldMap);
}