/**
 * Created by alexanderbelenov on 30.05.2022.
 */

public with sharing class VacancyService implements IVacancyService {
    private final VacancyShare vacancyShare = new VacancyShare();
    public void updateAccessFor(Map<Id, Vacancy__c> newMap, Map<Id, Vacancy__c> oldMap, String accessLevel) {
        this.grantAccessFor(newMap, accessLevel);
        this.removeAccessFor(oldMap);
    }
    public void grantAccessFor(Map<Id, Vacancy__c> vacancyMap, String accessLevel) {
        final List<VacancyShareDTO> toGrantList = this.getShareInfoToGrantOrDenied(vacancyMap, accessLevel);
        this.vacancyShare.grandAccess(toGrantList);
    }
    public void removeAccessFor(Map<Id, Vacancy__c> vacancyMap) {
        final List<VacancyShareDTO> toRemoveList = this.getShareInfoToGrantOrDenied(vacancyMap, null);
        this.vacancyShare.removeAccess(toRemoveList);
    }
    private List<VacancyShareDTO> getShareInfoToGrantOrDenied (Map<Id, Vacancy__c> vacancyMap, String accessLevel) {
        final List<VacancyShareDTO> result = new List<VacancyShareDTO>();
        final Map<Id, Manager__c> assigneesMap = this.getVacanciesAssignees(vacancyMap);
        for (Id vacancyKey : vacancyMap.keySet()) {
            final Vacancy__c vacancy = vacancyMap.get(vacancyKey);
            if (vacancy.Assignee__c == null) { continue; }
            final Manager__c manager = assigneesMap.get(vacancy.Assignee__c);
            result.add(new VacancyShareDTO(
                    manager.OwnerId,
                    vacancy.Id,
                    accessLevel
            ));
        }
        return result;
    }
    private Map<Id, Manager__c> getVacanciesAssignees(Map<Id, Vacancy__c> vacancyMap) {
        final List<Id> assigneeIds = new List<Id>();
        for (Vacancy__c vacancy : vacancyMap.values()) {
            assigneeIds.add(vacancy.Assignee__c);
        }

        Map<Id, Manager__c> managersOfVacancies = new Map<Id, Manager__c>([
                SELECT Id, OwnerId
                FROM Manager__c
                WHERE Id In :assigneeIds
        ]);

        return managersOfVacancies;
    }
}