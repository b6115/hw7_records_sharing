/**
 * Created by alexanderbelenov on 30.05.2022.
 */

trigger VacancyTrigger on Vacancy__c (
        before insert, before update, before delete,
        after insert, after update, after delete, after undelete) {

    final IVacancyTriggerHandler handler = VacancyTriggerHandler.getInstance();

    if (Trigger.isAfter && Trigger.isInsert) {
        handler.afterInsert(Trigger.newMap);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
        handler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
    }

}